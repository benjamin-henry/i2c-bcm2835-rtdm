# C librairy for low level i2c on bcm2835 and related

----

## Disclaimer

- Works only if bcm2835-rtdm module is enabled
- You should be root to execute comp_run.sh

----

## How to

Run comp_run.sh to compile and install module and executable


## Description

If you successfully insert the i2c-rtdm-bcm2835 module, you should periodically see a log message in dmesg.
You can also test the driver in the userspace by executing ./i2c-rtdm-dev. For now, the opening and writing operations are successful, but the readings gets the error : **"i2c_bcm2835_rtdm_read_rt: Can't copy data from driver to user space (-14)!"**. This still has to be fixed.