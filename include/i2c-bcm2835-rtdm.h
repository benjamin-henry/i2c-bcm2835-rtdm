/**
 * Copyright (C) 2020 Timothée Simon <timothee.simon@utt.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BCM283X_I2C_RTDM_H
#define BCM283X_I2C_RTDM_H

/**
 * Maximum size for transmit and receive buffers.
 */
#define BCM283X_I2C_BUFFER_SIZE_MAX 1024

/**
 * IOCTL request for changing the I2C bus speed.
 */
#define BCM283X_I2C_SET_SPEED 2500

#define BCM283X_I2C_DEFAULT_ADRESS 0x68
// #define BCM283X_I2C_DEFAULT_ADRESS 0x03

#define BCM283X_I2C_DEFAULT_BAUDRATE 100000

/**
 * List of available speeds for the I2C bus.
 */
typedef enum
{
    BCM283x_I2C_SPEED_100kHz    = 2500,      /*!< 2500 = 10us = 100 kHz */
    BCM283x_I2C_SPEED_399kHz    = 626,       /*!< 622 = 2.504us = 399.3610 kHz */
    BCM283x_I2C_SPEED_1_666MHz  = 150,       /*!< 150 = 60ns = 1.666 MHz (default at reset) */
    BCM283x_I2C_SPEED_1_689MHz  = 148        /*!< 148 = 59ns = 1.689 MHz */
} bcm283x_i2c_speed_e;

/*! \brief bcm2835I2CReasonCodes
  Specifies the reason codes for the bcm2835_i2c_write and bcm2835_i2c_read functions.
*/
typedef enum
{
    BCM283x_I2C_REASON_OK   	     = 0x00,      /*!< Success */
    BCM283x_I2C_REASON_ERROR_NACK    = 0x01,      /*!< Received a NACK */
    BCM283x_I2C_REASON_ERROR_CLKT    = 0x02,      /*!< Received Clock Stretch Timeout */
    BCM283x_I2C_REASON_ERROR_DATA    = 0x04       /*!< Not all data is sent / received */
} bcm283x_i2c_reason_codes_e;


#endif /* BCM283X_I2C_RTDM_H */