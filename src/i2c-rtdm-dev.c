#include <stdio.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define CLEAR_STDIN { for(int c = 0; c!='\n' && c!=EOF; c = getchar()); }

#define RTDM_NAME "/dev/rtdm/i2cdev"


// int customPeriod(void) {
// 	int rep, nbr;

// 	printf("> Enter your custom period (us) :\n");
// 	rep = scanf("%d",&nbr);
// 	CLEAR_STDIN;

// 	if (rep <= 0) {
// 		printf("Please enter only digits\n");
// 		return customPeriod();
// 	}

// 	return nbr;
// }

// int menu(void) {
// 	int rep, nbr, choice;

// 	printf("> Pick a period (in us) :\n  [1] - 10\n  [2] - 20\n  [3] - 30\n  [4] - 40\n  [5] - 50\n  [6] - Custom\n");
// 	rep = scanf("%d",&nbr);
// 	CLEAR_STDIN;

// 	if (rep <= 0) {
// 		printf("Please enter only digits\n");
// 		return menu();
// 	} else {
// 		switch(nbr) {
// 			case 1:
// 				choice = 10;
// 				break;
// 			case 2:
// 				choice = 20;
// 				break;
// 			case 3:
// 				choice = 30;
// 				break;
// 			case 4:
// 				choice = 40;
// 				break;
// 			case 5:
// 				choice = 50;
// 				break;
// 			case 6:
// 				choice = customPeriod();
// 				break;
// 			default:
// 				printf("Wrong choice !\n");
// 				return menu();
// 				break;
// 		}
// 	}
	
// 	return choice;
// }

// char looper(void) {
// 	char buffer[1];
// 	int rep, choice;

// 	printf("> Would you like to change it again ? (y)/(n)\n");
// 	rep = scanf("%s", buffer);
// 	CLEAR_STDIN;
	
// 	if (rep <= 0) {
// 		printf("Please enter y or n\n");
// 		return looper();
// 	} else {
// 		if (!strcmp(buffer,"y")) {
// 			choice = 1;
// 		} else if (!strcmp(buffer,"n")) {
// 			choice = 0;
// 		} else {
// 			printf("Please enter y or n\n");
// 			return looper();
// 		}
// 	}

// 	return choice;
// }

// void cut_uint_to_char(int i, char c[4]) {
// 	c[0] = (char) (i>>24);
// 	c[1] = (char) (i>>16);
// 	c[2] = (char) (i>>8);
// 	c[3] = (char) i;
// }

int main(void)
{
	char loop = 1, msg[4];
	int fd, ret, new_period, errnum;

	printf("test started\n");

	fd = open(RTDM_NAME, O_RDWR );
	if (fd < 0)	printf("open error\n");
	else {
		printf("succesfully opened\n");
		const void* test = "1111";
		ret = write(fd, test, sizeof(test));
		// ret = write(fd, msg, sizeof(msg));
		if (ret <= 0) {
			// errnum = errno;
			// fprintf(stderr, "Error writing : %s\n", strerror( errnum ));
			printf("write error\n");
		}

		printf("succesfully written\n");


		void* reg = "0x3C";
		ret = read(fd, reg, sizeof(reg));
		if (ret < 0)	printf("read error\n");
		else {
			printf("Read value : %d\n", ret);
		}
		

		// do {
		// 	new_period = menu();
		// 	printf("> You've chosen a period of %d us\n", new_period);

		// 	cut_uint_to_char(new_period,msg);
		// 	ret = write(fd, msg, sizeof(msg));
		// 	if (ret <= 0) {
		// 		// errnum = errno;
		// 		// fprintf(stderr, "Error writing : %s\n", strerror( errnum ));
		// 		printf("write error\n");
		// 	}
		// } while (looper());
	}
}
