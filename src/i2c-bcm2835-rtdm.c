
/**
 * Copyright (C) 2020 Timothée Simon <timothee.simon@utt.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* Self header */
#include "../include/i2c-bcm2835-rtdm.h"

/* Linux headers */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/printk.h>
#include <linux/init.h>
#include <linux/errno.h>

/* RTDM headers */
#include <rtdm/rtdm.h>
#include <rtdm/driver.h>

/* BCM2835 library header */
// #include <bcm2835-rtdm.h> // TODO : include proprement bcm2835-rtdm.h
#include "../../bcm2835-rtdm/include/bcm2835-rtdm.h"

/* Bypass eclipse syntax checker */
#ifdef __CDT_PARSER__
	#define __init
	#define __exit
#endif /* __CDT_PARSER__ */

/* Timer */
static uint period_us = 1000000;
module_param(period_us, int, 0644);
static void timerIT(rtdm_timer_t *);
static rtdm_timer_t rtimer;

static int init_i2c_timer(void) {
    int err;
	
	if ((err = rtdm_timer_init(&rtimer, timerIT, "Timer")) != 0) {
		return err;
	}

	if ((err = rtdm_timer_start(&rtimer, period_us*1000, period_us*1000, RTDM_TIMERMODE_RELATIVE)) != 0) {
		rtdm_timer_destroy(& rtimer);
		return err;
	}

    return 0;
}

static void exit_i2c_timer(void)
{
	rtdm_timer_stop(&rtimer);
	rtdm_timer_destroy(&rtimer);
}


static void timerIT(rtdm_timer_t * unused) {
	static uint32_t len = 1;
	static char test = 0;
	static char addr[1] = {0x3c | 0x1};
	static uint8_t err;

	char buf[1] = {0};//,0,0,0};

	switch (test)
	{
	case 0:
		err = bcm2835_i2c_read(buf, len);
		rtdm_printk("----------------");
		rtdm_printk("slave %x addr %x", BCM283X_I2C_DEFAULT_ADRESS, addr[0]);
		// rtdm_printk("Read: %d %d %d %d from %d", (uint) buf[3], (uint) buf[2], (uint) buf[1], (uint) buf[0], BCM283X_I2C_DEFAULT_ADRESS);
		// rtdm_printk("(0)  %d %d %d %d (err: %d)", (uint) buf[3], (uint) buf[2], (uint) buf[1], (uint) buf[0], err);
		rtdm_printk("%d (err: %d)", buf[0], err);
		break;
	case 1:
		err = bcm2835_i2c_read_register_rs(addr, buf, len);
		// rtdm_printk("Read: %d %d %d %d from %d reg %d", (uint) buf[3], (uint) buf[2], (uint) buf[1], (uint) buf[0], BCM283X_I2C_DEFAULT_ADRESS, addr[0]);
		// rtdm_printk("(1)  %d %d %d %d (err: %d)", (uint) buf[3], (uint) buf[2], (uint) buf[1], (uint) buf[0], err);
		rtdm_printk("%d (err: %d)", buf[0], err);
		break;
	case 2:
		err = bcm2835_i2c_write_read_rs(addr, 1, buf, len);
		// rtdm_printk("Read: %d %d %d %d from %d reg %d", (uint) buf[3], (uint) buf[2], (uint) buf[1], (uint) buf[0], BCM283X_I2C_DEFAULT_ADRESS, addr[0]);
		// rtdm_printk("(2)  %d %d %d %d (err: %d)", (uint) buf[3], (uint) buf[2], (uint) buf[1], (uint) buf[0], err);
		rtdm_printk("%d (err: %d)", buf[0], err);
		break;
	case 3:
		bcm2835_i2c_write(addr, 1);
		delayMicroseconds(10);
		// bcm2835_i2c_write(buf, len);
		bcm2835_i2c_read(buf, len);
		// rtdm_printk("Read: %d %d %d %d from %d reg %d", (uint) buf[3], (uint) buf[2], (uint) buf[1], (uint) buf[0], BCM283X_I2C_DEFAULT_ADRESS, addr[0]);
		// rtdm_printk("(3)  %d %d %d %d", (uint) buf[3], (uint) buf[2], (uint) buf[1], (uint) buf[0]);
		break;
	
	default:
		break;
	}

	test = (test + 1) % 3;
}

/**
 * Buffer type.
 */
typedef struct buffer_s {
	int size;
	char data[BCM283X_I2C_BUFFER_SIZE_MAX];
} buffer_t;

/**
 * Device config structure stored inside each context.
 */
typedef struct config_s {
	int bit_order;
	int data_mode;
	int clock_divider;
	int chip_select;
	int chip_select_polarity;
} config_t;


/**
 * Device context, associated with every open device instance.
 */
typedef struct i2c_bcm2835_context_s {
	config_t config;
	buffer_t transmit_buffer;
	buffer_t receive_buffer;
} i2c_bcm2835_context_t;

/**
 * Open handler. Note: opening a named device instance always happens from secondary mode.
 * @param[in] fd File descriptor associated with opened device instance.
 * @param[in] oflags Open flags as passed by the user.
 * @return 0 on success. On failure, a negative error code is returned.
 */
static int i2c_bcm2835_rtdm_open(struct rtdm_fd *fd, int oflags) {

	i2c_bcm2835_context_t *context;

	/* Retrieve context */
	context = (i2c_bcm2835_context_t *) rtdm_fd_to_private(fd);

	/* Set default config */
	context->config.clock_divider = BCM2835_I2C_CLOCK_DIVIDER_150;
    context->config.chip_select = rtdm_fd_minor(fd);

	return 0;
}

/**
 * Close handler. Note: closing a device instance always happens from secondary mode.
 * @param[in] fd File descriptor associated with opened device instance.
 * @return 0 on success. On failure return a negative error code.
 */
static void i2c_bcm2835_rtdm_close(struct rtdm_fd *fd) {

	return;
}

/**
 * Read from the device.
 * @param[in] fd File descriptor.
 * @param[out] buf Input buffer as passed by the user.
 * @param[in] size Number of bytes the user requests to read.
 * @return On success, the number of bytes read. On failure return either -ENOSYS, to request that this handler be called again from the opposite realtime/non-realtime context, or another negative error code.
 */
static ssize_t i2c_bcm2835_rtdm_read_rt(struct rtdm_fd *fd, void __user *buf, size_t size) {

	i2c_bcm2835_context_t *context;
	size_t read_size;
	int res;

	/* Retrieve context */
	context = (i2c_bcm2835_context_t *) rtdm_fd_to_private(fd);

	/* Limit size */
	read_size = (size > BCM283X_I2C_BUFFER_SIZE_MAX) ? BCM283X_I2C_BUFFER_SIZE_MAX : size;
	read_size = (read_size > context->receive_buffer.size) ? context->receive_buffer.size : read_size;

	/* Copy data to user space */
	res = rtdm_safe_copy_to_user(fd, buf, (const void *) context->receive_buffer.data, read_size);
	if (res) {
		rtdm_printk(KERN_ERR "%s: Can't copy data from driver to user space (%d)!\r\n", __FUNCTION__, res);
		return (res < 0) ? res : -res;
	}

	/* Reset buffer size */
	context->receive_buffer.size = 0;

	/* Return read bytes */
	return read_size;
}

/**
 * Write to the device.
 * @warning If unread data was present in the receive buffer, it will be overwritten.
 * @param[in] fd File descriptor.
 * @param[in] buf Output buffer as passed by the user.
 * @param[in] size Number of bytes the user requests to write.
 * @return On success, the number of bytes written. On failure return either -ENOSYS, to request that this handler be called again from the opposite realtime/non-realtime context, or another negative error code.
 */
static ssize_t i2c_bcm2835_rtdm_write_rt(struct rtdm_fd *fd, const void __user *buf, size_t size) {

	i2c_bcm2835_context_t *context;
	size_t write_size;
	int res;

	/* Ensure that there will be enough space in the buffer */
	if (size > BCM283X_I2C_BUFFER_SIZE_MAX) {
		rtdm_printk(KERN_ERR "%s: Trying to transmit data larger than buffer size !", __FUNCTION__);
		return -EINVAL;
	}
	write_size = size;

	/* Retrieve context */
	context = (i2c_bcm2835_context_t *) rtdm_fd_to_private(fd);

	/* Save data in kernel space buffer */
	res = rtdm_safe_copy_from_user(fd, context->transmit_buffer.data, buf, write_size);
    rtdm_printk("%s: Received %d bytes : %d\n", __FUNCTION__, size, res);
	if (res) {
		rtdm_printk(KERN_ERR "%s: Can't copy data from user space to driver (%d)!\r\n", __FUNCTION__, res);
		return (res < 0) ? res : -res;
	}
	context->transmit_buffer.size = write_size;

	/* Warn if receive buffer was not empty */
	if (context->receive_buffer.size > 0) {
		rtdm_printk(KERN_WARNING "%s: Receive buffer was not empty and will be overwritten.\r\n", __FUNCTION__);
	}

	/* Restore device i2c settings */
    bcm2835_i2c_setClockDivider(context->config.clock_divider);
	
	/*
	bcm2835_spi_setBitOrder(context->config.bit_order);
	bcm2835_spi_setDataMode(context->config.data_mode);
	bcm2835_spi_setClockDivider(context->config.clock_divider);
	bcm2835_spi_setChipSelectPolarity(context->config.chip_select, context->config.chip_select_polarity);
	*/

	/* Initiate an outgoing transfer which will also store read content in input buffer. */
	/*
	bcm2835_spi_chipSelect(context->config.chip_select);
	bcm2835_spi_transfernb(context->transmit_buffer.data, context->receive_buffer.data, write_size);
	*/

	context->receive_buffer.size = write_size;
	/* Return bytes written */
	return write_size;
}



/**
 * Changes the clock divider setting for one device.
 * @param context The context associated with the device.
 * @param value An integer representing a clock divider preference.
 * @return 0 on success, -EINVAL if the specified value is invalid.
 */
static int i2c_bcm2835_change_clock_divider(i2c_bcm2835_context_t *context, const int value) {

	switch (value) {
		case BCM283x_I2C_SPEED_100kHz:
        case BCM283x_I2C_SPEED_399kHz:
        case BCM283x_I2C_SPEED_1_666MHz:
        case BCM283x_I2C_SPEED_1_689MHz:
			rtdm_printk(KERN_DEBUG "%s: Changing clock divider to %d.\r\n", __FUNCTION__, value);
			context->config.clock_divider = value;
			return 0;
	}

	rtdm_printk(KERN_ERR "%s: Unexpected value!\r\n", __FUNCTION__);
	return -EINVAL;
}

/**
 * IOCTL handler.
 * @param[in] fd File descriptor.
 * @param[in] request Request number as passed by the user.
 * @param[in,out] arg Request argument as passed by the user.
 * @return A positive value or 0 on success. On failure return either -ENOSYS, to request that the function be called again from the opposite realtime/non-realtime context, or another negative error code.
 */
static int i2c_bcm2835_ioctl_rt(struct rtdm_fd *fd, unsigned int request, void __user *arg) {

	i2c_bcm2835_context_t *context;
	int value;
	int res;

	/* Retrieve context */
	context = (i2c_bcm2835_context_t *) rtdm_fd_to_private(fd);

	/* Speed */
	/*
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
	bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_65536);
	bcm2835_spi_chipSelect(BCM2835_SPI_CS_NONE);
	 */


	/* Analyze request */
	switch (request) {

		case BCM283X_I2C_SET_SPEED: /* Change the bus speed */
			res = rtdm_safe_copy_from_user(fd, &value, arg, sizeof(int));
			if (res) {
				rtdm_printk(KERN_ERR "%s: Can't retrieve argument from user space (%d)!\r\n", __FUNCTION__, res);
				return (res < 0) ? res : -res;
			}
			return i2c_bcm2835_change_clock_divider(context, value);
			// return 1;

		default: /* Unexpected case */
			rtdm_printk(KERN_ERR "%s: Unexpected request : %d!\r\n", __FUNCTION__, request);
			return -EINVAL;

	}

}

/**********************************************************************************************************************
 * RTDM def
 */
 
/**
 * This structure describes the RTDM driver.
 */
static struct rtdm_driver i2c_bcm2835_driver = {
	// .profile_info = RTDM_PROFILE_INFO(i2c, RTDM_CLASS_TESTING, 1, 1),
	// .device_flags   = RTDM_NAMED_DEVICE,
	// .device_count   = 1,

	.profile_info = RTDM_PROFILE_INFO(i2c, RTDM_CLASS_EXPERIMENTAL, RTDM_SUBCLASS_GENERIC, 42),
	.device_flags = RTDM_NAMED_DEVICE | RTDM_EXCLUSIVE | RTDM_FIXED_MINOR,
	.device_count = 2,
	.context_size = sizeof(struct i2c_bcm2835_context_s),
	.ops = {
		.open = i2c_bcm2835_rtdm_open,
		.read_rt = i2c_bcm2835_rtdm_read_rt,
		// .read_nrt = i2c_bcm2835_rtdm_read_nrt,
		.write_rt = i2c_bcm2835_rtdm_write_rt,
		// .write_nrt = i2c_bcm2835_rtdm_write_nrt,
		.ioctl_rt = i2c_bcm2835_ioctl_rt,
		// .ioctl_nrt = i2c_bcm2835_ioctl_nrt,
		.close = i2c_bcm2835_rtdm_close
	}
};

/**
 * This structure contains the RTDM device created for I2C.
 */
static struct rtdm_device i2c_bcm283x_device = {
	/* Prepare to register the device */
    /* Set device parameters */
    // i2c_bcm283x_device.minor = 0,
	.driver = &i2c_bcm2835_driver,
	.label  = "i2cdev",
};

/**
 * This function is called when the module is loaded. It initializes the
 * i2c device using the bcm2835 libary, and registers the RTDM device.
 */
static int __init i2c_bcm2835_rtdm_init(void) {

	int res;

	/* Log */
	rtdm_printk(KERN_INFO "%s: Starting driver ...", __FUNCTION__);

	/* Ensure cobalt is enabled */
	if (!realtime_core_enabled()) {
		rtdm_printk(KERN_ERR "%s: Exiting as cobalt is not enabled!\r\n", __FUNCTION__);
		return -1;
	}

	/* Initialize the bcm2835 library */
	res = bcm2835_init();
	if (res != 1) {
		rtdm_printk(KERN_ERR "%s: Error in bcm2835_init (%d).\r\n", __FUNCTION__, res);
		return -1;
	}

	/* Configure the i2c port from bcm2835 library with arbitrary settings */
	bcm2835_i2c_begin();
	rtdm_printk("%s: starting i2c",  __FUNCTION__);
    bcm2835_i2c_setSlaveAddress(BCM283X_I2C_DEFAULT_ADRESS);
	rtdm_printk("%s: set slave addr at %x",  __FUNCTION__, BCM283X_I2C_DEFAULT_ADRESS);
	// bcm2835_i2c_setClockDivider(BCM2835_I2C_CLOCK_DIVIDER_150);
	// rtdm_printk("%s: set clock divider",  __FUNCTION__);
	bcm2835_i2c_set_baudrate(BCM283X_I2C_DEFAULT_BAUDRATE);
	rtdm_printk("%s: set baudrate at %d Hz",  __FUNCTION__, BCM283X_I2C_DEFAULT_BAUDRATE);

    /* Try to register the device */
    res = rtdm_dev_register(&i2c_bcm283x_device);
    if (res == 0) {
        rtdm_printk(KERN_INFO "%s: Device %s. registered without errors.\r\n", __FUNCTION__, i2c_bcm283x_device.label);
    } else {
        rtdm_printk(KERN_ERR "%s: Device %s. registration failed : ", __FUNCTION__, i2c_bcm283x_device.label);
        switch (res) {
            case -EINVAL:
                rtdm_printk(KERN_ERR "The descriptor contains invalid entries.\r\n");
                break;

            case -EEXIST:
                rtdm_printk(KERN_ERR "The specified device name of protocol ID is already in use.\r\n");
                break;

            case -ENOMEM:
                rtdm_printk(KERN_ERR "A memory allocation failed in the process of registering the device.\r\n");
                break;

            default:
                rtdm_printk(KERN_ERR "Unknown error code returned.\r\n");
                break;
        }
        return res;
	}

	/* Init Timer */
	init_i2c_timer();

	return 0;
}

/**
 * This function is called when the module is unloaded. It unregisters the RTDM device.
 */
static void __exit i2c_bcm2835_rtdm_exit(void) {


	/* Log */
	rtdm_printk(KERN_INFO "%s: Stopping driver ...\r\n", __FUNCTION__);

	/* Ensure cobalt is enabled */
	if (!realtime_core_enabled()) {
		rtdm_printk(KERN_ERR "%s: Exiting as cobalt is not enabled!\r\n", __FUNCTION__);
		return;
	}

	/* Stop timer */
	exit_i2c_timer();

	/* Unregister the device */
    rtdm_printk(KERN_INFO "%s: Unregistering device 0 ...\r\n", __FUNCTION__);
    rtdm_dev_unregister(&i2c_bcm283x_device);
	

	/* Release the i2c pins */
	bcm2835_i2c_end();

	/* Unmap memory */
	bcm2835_close();

	/* Log */
	rtdm_printk(KERN_INFO "%s: All done!\r\n", __FUNCTION__);

}

/*
 * Link init and exit functions with driver entry and exit points.
 */
module_init(i2c_bcm2835_rtdm_init);
module_exit(i2c_bcm2835_rtdm_exit);

MODULE_DESCRIPTION("Real-Time I2C driver for the Broadcom BCM283x SoC familly using the RTDM API");
MODULE_AUTHOR("Timothée Simon <timothee.simon@utt.fr>");
MODULE_LICENSE("GPL");