#!/bin/bash

rmmod i2c-bcm2835-rtdm
# rmmod bcm2835-rtdm

sleep 0.5

make

# insmod bcm2835-rtdm.ko
insmod i2c-bcm2835-rtdm.ko

sleep 0.5

dmesg | tail -n 10