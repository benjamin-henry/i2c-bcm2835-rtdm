# TODO : include coreectement le bcm2835-rtdm.h

BCM_LIB_DIR := ../bcm2835-rtdm/include
INSTALL_DIR ?= .
SRC_DIR := src
INCLUDE_DIR := include
KERNEL_DIR ?= /usr/src/linux
PWD ?= $(shell pwd)

EXTRA-FLAGS := -I$(KERNEL_DIR)/include/xenomai -I$(SRC_DIR) -I$(INCLUDE_DIR) -I$(BCM_LIB_DIR)
obj-m += $(SRC_DIR)/i2c-bcm2835-rtdm.o

ifeq ($(KERNELRELEASE),)
XENOCONFIG=/usr/xenomai/bin/xeno-config
CC=$(shell      $(XENOCONFIG) --cc)
CFLAGS=$(shell  $(XENOCONFIG) --skin=posix --cflags)
LDFLAGS=$(shell $(XENOCONFIG) --skin=posix --ldflags)
LIBDIR=$(shell  $(XENOCONFIG) --skin=posix --libdir)
endif

.PHONY: all modules executable install info

all: modules executable install info

modules:
	$(MAKE) -C $(KERNEL_DIR) M=$(PWD) modules

executable: 
	$(CC) -c $(SRC_DIR)/i2c-rtdm-dev.c -o $(SRC_DIR)/i2c-rtdm-dev.o $(CFLAGS) $(LDFLAGS)
	/usr/xenomai/bin/wrap-link.sh -v $(CC) -o $(INSTALL_DIR)/i2c-rtdm-dev $(SRC_DIR)/i2c-rtdm-dev.o $(LDFLAGS)

install:
	mv $(SRC_DIR)/i2c-bcm2835-rtdm.ko $(PWD)

info:
	modinfo i2c-bcm2835-rtdm.ko

clean:
	make -C $(KERNEL_DIR) M=$(PWD) clean
	rm -f $(SRC_DIR)/*.o $(SRC_DIR)/.*.o.*
